package chaplapp

import (
	"time"
	"fmt"
	"io"
	"strings"
)

const (
	meetingDateFormat = "02/01/06"
	configDir        = ".chaplapp"
	chairmanFileName = "chairmen.json"
	meetingFileName  = "meetings.json"
)

type chairman struct {
	firstname, lastname string
}

func (chairman chairman) String () string {
	return fmt.Sprint(chairman.firstname, " ", chairman.lastname)
}

type meeting struct {
	time time.Time
}

func (meeting meeting) String () string {
	return meeting.time.Format(meetingDateFormat)
}

type assignment struct {
	chairman *chairman
	meeting *meeting
}

type chairmanList []chairman;

func (chairmen chairmanList) Len() int { 
	return len(chairmen) 
}

func (chairmen chairmanList) Swap(left, right int) { 
	leftChairman := chairmen[left]
	chairmen[left] = chairmen[right]
	chairmen[right] = leftChairman
}

func (chairmen chairmanList) Less(left, right int) bool {
	if strings.Compare(chairmen[left].lastname, chairmen[right].lastname) < 0 {
		return true
	} 
	return strings.Compare(chairmen[left].firstname, chairmen[right].firstname) < 0 
}

type chairmanProvider func() []chairman

type meetingProvider func() []meeting

type planningStrategy func(chairmanProvider, meetingProvider) ([]assignment, error)

type renderer func (io.Writer, planningStrategy)
