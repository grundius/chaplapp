package chaplapp

import (
    "testing"
    "time"
    "sort"
)

func TestChairmanStringRepresentation(t *testing.T) {
	chairman := chairman {"foo","bar"}
	
	if fullname := chairman.String(); fullname != "foo bar"{
		t.Error("Incorrect String representation: [", fullname, "]; should be [foo bar]")
	}
}

func TestMeetingStringRepresentation(t *testing.T) {
	time := time.Now()
	expected := time.Format(meetingDateFormat)
	meeting := meeting {time: time}
	
	if result := meeting.String(); result != expected{
		t.Error("Incorrect String representation: [", result, "]; should be [", expected, "]")
	}
}

func TestChairmenGetSortedAlphabetically(t *testing.T) {
	chairmen := []chairman { chairman {"foo", "bar"}, chairman {"john", "doe"}, chairman {"jane", "doe"}, chairman { "shouldbefirst", "aaa" }}
	source := make([]chairman, 4)
	copy(chairmen, source)
	chairmanList := chairmanList (source)
	
	sort.Sort(chairmanList)
	
	checkChairman(chairmanList, 0, chairmen[3],t)
	checkChairman(chairmanList, 1, chairmen[0],t)
	checkChairman(chairmanList, 2, chairmen[2],t)
	checkChairman(chairmanList, 3, chairmen[1],t)
}

func checkChairman(list chairmanList, index int, expected chairman, t *testing.T) {
	if chairman :=  list[index]; chairman.firstname != expected.firstname || chairman.lastname != expected.lastname {
		t.Error("chairman[", index, "] should be [", expected, "] but is [", chairman, "]")
	}
}
